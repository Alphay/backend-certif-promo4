-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: my_eDatabase
-- ------------------------------------------------------
-- Server version	5.7.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Products`
--

DROP TABLE IF EXISTS `Products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Products`
--

LOCK TABLES `Products` WRITE;
/*!40000 ALTER TABLE `Products` DISABLE KEYS */;
INSERT INTO `Products` VALUES (1,'Huggo boss 0','montre femme',99,'2019-01-26 17:57:47','2019-01-26 17:57:47'),(2,'Huggo boss 1','montre femme',99,'2019-01-26 17:57:47','2019-01-26 17:57:47'),(3,'Huggo boss 2','montre femme',99,'2019-01-26 17:57:47','2019-01-26 17:57:47'),(4,'Huggo boss 3','montre femme',99,'2019-01-26 17:57:47','2019-01-26 17:57:47'),(5,'Huggo boss 4','montre femme',99,'2019-01-26 17:57:47','2019-01-26 17:57:47'),(6,'Huggo boss 5','montre femme',99,'2019-01-26 17:57:47','2019-01-26 17:57:47'),(7,'Huggo boss 6','montre femme',99,'2019-01-26 17:57:47','2019-01-26 17:57:47'),(8,'Huggo boss 7','montre femme',99,'2019-01-26 17:57:47','2019-01-26 17:57:47'),(9,'Huggo boss 8','montre femme',99,'2019-01-26 17:57:47','2019-01-26 17:57:47'),(10,'Huggo boss 9','montre femme',99,'2019-01-26 17:57:47','2019-01-26 17:57:47'),(11,'Huggo boss 10','montre femme',99,'2019-01-26 17:57:47','2019-01-26 17:57:47'),(12,'Huggo boss 11','montre femme',99,'2019-01-26 17:57:47','2019-01-26 17:57:47'),(13,'Huggo boss 12','montre femme',99,'2019-01-26 17:57:47','2019-01-26 17:57:47'),(14,'Huggo boss 13','montre femme',99,'2019-01-26 17:57:47','2019-01-26 17:57:47'),(15,'Huggo boss 14','montre femme',99,'2019-01-26 17:57:47','2019-01-26 17:57:47'),(16,'montre argentée','bijoux femme',89,'2019-01-26 17:57:47','2019-01-26 17:57:47'),(17,'Huggo boss 0','montre femme',99,'2019-01-26 18:03:22','2019-01-26 18:03:22'),(18,'Huggo boss 1','montre femme',99,'2019-01-26 18:03:22','2019-01-26 18:03:22'),(19,'Huggo boss 2','montre femme',99,'2019-01-26 18:03:22','2019-01-26 18:03:22'),(20,'Huggo boss 3','montre femme',99,'2019-01-26 18:03:22','2019-01-26 18:03:22'),(21,'Huggo boss 4','montre femme',99,'2019-01-26 18:03:22','2019-01-26 18:03:22'),(22,'Huggo boss 5','montre femme',99,'2019-01-26 18:03:22','2019-01-26 18:03:22'),(23,'Huggo boss 6','montre femme',99,'2019-01-26 18:03:22','2019-01-26 18:03:22'),(24,'Huggo boss 7','montre femme',99,'2019-01-26 18:03:22','2019-01-26 18:03:22'),(25,'Huggo boss 8','montre femme',99,'2019-01-26 18:03:22','2019-01-26 18:03:22'),(26,'Huggo boss 9','montre femme',99,'2019-01-26 18:03:22','2019-01-26 18:03:22'),(27,'Huggo boss 10','montre femme',99,'2019-01-26 18:03:22','2019-01-26 18:03:22'),(28,'Huggo boss 11','montre femme',99,'2019-01-26 18:03:22','2019-01-26 18:03:22'),(29,'Huggo boss 12','montre femme',99,'2019-01-26 18:03:22','2019-01-26 18:03:22'),(30,'Huggo boss 13','montre femme',99,'2019-01-26 18:03:22','2019-01-26 18:03:22'),(31,'Huggo boss 14','montre femme',99,'2019-01-26 18:03:22','2019-01-26 18:03:22'),(32,'montre argentée','bijoux femme',89,'2019-01-26 18:03:22','2019-01-26 18:03:22');
/*!40000 ALTER TABLE `Products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SequelizeMeta`
--

DROP TABLE IF EXISTS `SequelizeMeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SequelizeMeta` (
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SequelizeMeta`
--

LOCK TABLES `SequelizeMeta` WRITE;
/*!40000 ALTER TABLE `SequelizeMeta` DISABLE KEYS */;
INSERT INTO `SequelizeMeta` VALUES ('20190126141841-create-product.js'),('20190126142747-create-user.js'),('20190126143326-create-shopping-cart.js'),('20190126154200-create-to-buy.js');
/*!40000 ALTER TABLE `SequelizeMeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ShoppingCarts`
--

DROP TABLE IF EXISTS `ShoppingCarts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ShoppingCarts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `total` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `userId` int(11) NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  CONSTRAINT `ShoppingCarts_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `Users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ShoppingCarts`
--

LOCK TABLES `ShoppingCarts` WRITE;
/*!40000 ALTER TABLE `ShoppingCarts` DISABLE KEYS */;
INSERT INTO `ShoppingCarts` VALUES (1,19900,'2019-02-09 16:26:23',1,'2019-02-09 16:26:23'),(2,15500,'2019-02-09 16:26:23',5,'2019-02-09 16:26:23'),(3,13300,'2019-01-09 16:26:33',4,'2019-02-09 16:26:33'),(4,12200,'2019-02-09 16:26:33',3,'2019-02-09 16:26:33'),(5,11500,'2019-02-09 16:26:23',2,'2019-02-09 16:26:23');
/*!40000 ALTER TABLE `ShoppingCarts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ToBuys`
--

DROP TABLE IF EXISTS `ToBuys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ToBuys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idProduct` int(11) DEFAULT NULL,
  `idShoppingCart` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idProduct` (`idProduct`),
  KEY `idShoppingCart` (`idShoppingCart`),
  CONSTRAINT `ToBuys_ibfk_1` FOREIGN KEY (`idProduct`) REFERENCES `Products` (`id`),
  CONSTRAINT `ToBuys_ibfk_2` FOREIGN KEY (`idShoppingCart`) REFERENCES `ShoppingCarts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ToBuys`
--

LOCK TABLES `ToBuys` WRITE;
/*!40000 ALTER TABLE `ToBuys` DISABLE KEYS */;
INSERT INTO `ToBuys` VALUES (1,2,5,2,'2019-02-09 16:26:23','2019-02-09 16:26:23'),(2,4,3,3,'2019-01-09 18:26:34','2019-01-09 18:26:34'),(3,1,2,1,'2019-02-09 16:26:23','2019-02-09 16:26:23'),(4,5,1,4,'2019-02-07 16:26:23','2019-02-07 16:26:23'),(5,3,4,5,'2019-02-09 16:26:23','2019-02-09 16:26:23'),(6,2,5,3,'2019-01-05 19:26:23','2019-01-05 19:26:23');
/*!40000 ALTER TABLE `ToBuys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users`
--

LOCK TABLES `Users` WRITE;
/*!40000 ALTER TABLE `Users` DISABLE KEYS */;
INSERT INTO `Users` VALUES (1,'Kevin Doquero 0','kevin.doque.0@yopmail.com','$2b$12$79A0khC8GUyEADHwBLb9petIFpnPwPUsF1DO4oYqGk6sQOWa8HiSm','2019-01-26 17:43:39','2019-01-26 17:43:39'),(2,'Kevin Doquero 1','kevin.doque.1@yopmail.com','$2b$12$79A0khC8GUyEADHwBLb9petIFpnPwPUsF1DO4oYqGk6sQOWa8HiSm','2019-01-26 17:43:39','2019-01-26 17:43:39'),(3,'Kevin Doquero 2','kevin.doque.2@yopmail.com','$2b$12$79A0khC8GUyEADHwBLb9petIFpnPwPUsF1DO4oYqGk6sQOWa8HiSm','2019-01-26 17:43:39','2019-01-26 17:43:39'),(4,'Kevin Doquero 3','kevin.doque.3@yopmail.com','$2b$12$79A0khC8GUyEADHwBLb9petIFpnPwPUsF1DO4oYqGk6sQOWa8HiSm','2019-01-26 17:43:39','2019-01-26 17:43:39'),(5,'Kevin Doquero 4','kevin.doque.4@yopmail.com','$2b$12$79A0khC8GUyEADHwBLb9petIFpnPwPUsF1DO4oYqGk6sQOWa8HiSm','2019-01-26 17:43:39','2019-01-26 17:43:39'),(6,'Kevin Doquero 5','kevin.doque.5@yopmail.com','$2b$12$79A0khC8GUyEADHwBLb9petIFpnPwPUsF1DO4oYqGk6sQOWa8HiSm','2019-01-26 17:43:39','2019-01-26 17:43:39'),(7,'Kevin Doquero 6','kevin.doque.6@yopmail.com','$2b$12$79A0khC8GUyEADHwBLb9petIFpnPwPUsF1DO4oYqGk6sQOWa8HiSm','2019-01-26 17:43:39','2019-01-26 17:43:39'),(8,'Kevin Doquero 7','kevin.doque.7@yopmail.com','$2b$12$79A0khC8GUyEADHwBLb9petIFpnPwPUsF1DO4oYqGk6sQOWa8HiSm','2019-01-26 17:43:39','2019-01-26 17:43:39'),(9,'Kevin Doquero 8','kevin.doque.8@yopmail.com','$2b$12$79A0khC8GUyEADHwBLb9petIFpnPwPUsF1DO4oYqGk6sQOWa8HiSm','2019-01-26 17:43:39','2019-01-26 17:43:39'),(10,'Kevin Doquero 9','kevin.doque.9@yopmail.com','$2b$12$79A0khC8GUyEADHwBLb9petIFpnPwPUsF1DO4oYqGk6sQOWa8HiSm','2019-01-26 17:43:39','2019-01-26 17:43:39'),(11,'Kevin Doquero 10','kevin.doque.10@yopmail.com','$2b$12$79A0khC8GUyEADHwBLb9petIFpnPwPUsF1DO4oYqGk6sQOWa8HiSm','2019-01-26 17:43:39','2019-01-26 17:43:39'),(12,'Kevin Doquero 11','kevin.doque.11@yopmail.com','$2b$12$79A0khC8GUyEADHwBLb9petIFpnPwPUsF1DO4oYqGk6sQOWa8HiSm','2019-01-26 17:43:39','2019-01-26 17:43:39'),(13,'Kevin Doquero 12','kevin.doque.12@yopmail.com','$2b$12$79A0khC8GUyEADHwBLb9petIFpnPwPUsF1DO4oYqGk6sQOWa8HiSm','2019-01-26 17:43:39','2019-01-26 17:43:39'),(14,'Kevin Doquero 13','kevin.doque.13@yopmail.com','$2b$12$79A0khC8GUyEADHwBLb9petIFpnPwPUsF1DO4oYqGk6sQOWa8HiSm','2019-01-26 17:43:39','2019-01-26 17:43:39'),(15,'Kevin Doquero 14','kevin.doque.14@yopmail.com','$2b$12$79A0khC8GUyEADHwBLb9petIFpnPwPUsF1DO4oYqGk6sQOWa8HiSm','2019-01-26 17:43:39','2019-01-26 17:43:39'),(16,'admin','admin@yopmail.com','$2b$12$CHuObW3p05Usm3mAcBf8.u.U9AJ5edfIGqJ/HtJHl2PP0hX1xGkqW','2019-01-26 17:43:39','2019-01-26 17:43:39'),(17,'Kevin Doquero 0','kevin.doque.0@yopmail.com','$2b$12$QoSDElL9DzEPCu.J1v/0wO/jyx.hLqZMzJ1F/lxMkiUsRAb/1MVnW','2019-01-26 17:57:46','2019-01-26 17:57:46'),(18,'Kevin Doquero 1','kevin.doque.1@yopmail.com','$2b$12$QoSDElL9DzEPCu.J1v/0wO/jyx.hLqZMzJ1F/lxMkiUsRAb/1MVnW','2019-01-26 17:57:46','2019-01-26 17:57:46'),(19,'Kevin Doquero 2','kevin.doque.2@yopmail.com','$2b$12$QoSDElL9DzEPCu.J1v/0wO/jyx.hLqZMzJ1F/lxMkiUsRAb/1MVnW','2019-01-26 17:57:46','2019-01-26 17:57:46'),(20,'Kevin Doquero 3','kevin.doque.3@yopmail.com','$2b$12$QoSDElL9DzEPCu.J1v/0wO/jyx.hLqZMzJ1F/lxMkiUsRAb/1MVnW','2019-01-26 17:57:46','2019-01-26 17:57:46'),(21,'Kevin Doquero 4','kevin.doque.4@yopmail.com','$2b$12$QoSDElL9DzEPCu.J1v/0wO/jyx.hLqZMzJ1F/lxMkiUsRAb/1MVnW','2019-01-26 17:57:46','2019-01-26 17:57:46'),(22,'Kevin Doquero 5','kevin.doque.5@yopmail.com','$2b$12$QoSDElL9DzEPCu.J1v/0wO/jyx.hLqZMzJ1F/lxMkiUsRAb/1MVnW','2019-01-26 17:57:46','2019-01-26 17:57:46'),(23,'Kevin Doquero 6','kevin.doque.6@yopmail.com','$2b$12$QoSDElL9DzEPCu.J1v/0wO/jyx.hLqZMzJ1F/lxMkiUsRAb/1MVnW','2019-01-26 17:57:46','2019-01-26 17:57:46'),(24,'Kevin Doquero 7','kevin.doque.7@yopmail.com','$2b$12$QoSDElL9DzEPCu.J1v/0wO/jyx.hLqZMzJ1F/lxMkiUsRAb/1MVnW','2019-01-26 17:57:46','2019-01-26 17:57:46'),(25,'Kevin Doquero 8','kevin.doque.8@yopmail.com','$2b$12$QoSDElL9DzEPCu.J1v/0wO/jyx.hLqZMzJ1F/lxMkiUsRAb/1MVnW','2019-01-26 17:57:46','2019-01-26 17:57:46'),(26,'Kevin Doquero 9','kevin.doque.9@yopmail.com','$2b$12$QoSDElL9DzEPCu.J1v/0wO/jyx.hLqZMzJ1F/lxMkiUsRAb/1MVnW','2019-01-26 17:57:46','2019-01-26 17:57:46'),(27,'Kevin Doquero 10','kevin.doque.10@yopmail.com','$2b$12$QoSDElL9DzEPCu.J1v/0wO/jyx.hLqZMzJ1F/lxMkiUsRAb/1MVnW','2019-01-26 17:57:46','2019-01-26 17:57:46'),(28,'Kevin Doquero 11','kevin.doque.11@yopmail.com','$2b$12$QoSDElL9DzEPCu.J1v/0wO/jyx.hLqZMzJ1F/lxMkiUsRAb/1MVnW','2019-01-26 17:57:46','2019-01-26 17:57:46'),(29,'Kevin Doquero 12','kevin.doque.12@yopmail.com','$2b$12$QoSDElL9DzEPCu.J1v/0wO/jyx.hLqZMzJ1F/lxMkiUsRAb/1MVnW','2019-01-26 17:57:46','2019-01-26 17:57:46'),(30,'Kevin Doquero 13','kevin.doque.13@yopmail.com','$2b$12$QoSDElL9DzEPCu.J1v/0wO/jyx.hLqZMzJ1F/lxMkiUsRAb/1MVnW','2019-01-26 17:57:46','2019-01-26 17:57:46'),(31,'Kevin Doquero 14','kevin.doque.14@yopmail.com','$2b$12$QoSDElL9DzEPCu.J1v/0wO/jyx.hLqZMzJ1F/lxMkiUsRAb/1MVnW','2019-01-26 17:57:46','2019-01-26 17:57:46'),(32,'admin','admin@yopmail.com','$2b$12$PKoIeYzLWRRDbln0.qvfGOCaYskdMb5345ZOCViMVlqN/c53I4vlm','2019-01-26 17:57:47','2019-01-26 17:57:47'),(33,'Kevin Doquero 0','kevin.doque.0@yopmail.com','$2b$12$QfTyRbJm.QVsllbJ7t57JOtrubfo5FzvaMz6aFcZwR1.sp17g3oJq','2019-01-26 18:03:21','2019-01-26 18:03:21'),(34,'Kevin Doquero 1','kevin.doque.1@yopmail.com','$2b$12$QfTyRbJm.QVsllbJ7t57JOtrubfo5FzvaMz6aFcZwR1.sp17g3oJq','2019-01-26 18:03:21','2019-01-26 18:03:21'),(35,'Kevin Doquero 2','kevin.doque.2@yopmail.com','$2b$12$QfTyRbJm.QVsllbJ7t57JOtrubfo5FzvaMz6aFcZwR1.sp17g3oJq','2019-01-26 18:03:21','2019-01-26 18:03:21'),(36,'Kevin Doquero 3','kevin.doque.3@yopmail.com','$2b$12$QfTyRbJm.QVsllbJ7t57JOtrubfo5FzvaMz6aFcZwR1.sp17g3oJq','2019-01-26 18:03:21','2019-01-26 18:03:21'),(37,'Kevin Doquero 4','kevin.doque.4@yopmail.com','$2b$12$QfTyRbJm.QVsllbJ7t57JOtrubfo5FzvaMz6aFcZwR1.sp17g3oJq','2019-01-26 18:03:21','2019-01-26 18:03:21'),(38,'Kevin Doquero 5','kevin.doque.5@yopmail.com','$2b$12$QfTyRbJm.QVsllbJ7t57JOtrubfo5FzvaMz6aFcZwR1.sp17g3oJq','2019-01-26 18:03:21','2019-01-26 18:03:21'),(39,'Kevin Doquero 6','kevin.doque.6@yopmail.com','$2b$12$QfTyRbJm.QVsllbJ7t57JOtrubfo5FzvaMz6aFcZwR1.sp17g3oJq','2019-01-26 18:03:21','2019-01-26 18:03:21'),(40,'Kevin Doquero 7','kevin.doque.7@yopmail.com','$2b$12$QfTyRbJm.QVsllbJ7t57JOtrubfo5FzvaMz6aFcZwR1.sp17g3oJq','2019-01-26 18:03:21','2019-01-26 18:03:21'),(41,'Kevin Doquero 8','kevin.doque.8@yopmail.com','$2b$12$QfTyRbJm.QVsllbJ7t57JOtrubfo5FzvaMz6aFcZwR1.sp17g3oJq','2019-01-26 18:03:21','2019-01-26 18:03:21'),(42,'Kevin Doquero 9','kevin.doque.9@yopmail.com','$2b$12$QfTyRbJm.QVsllbJ7t57JOtrubfo5FzvaMz6aFcZwR1.sp17g3oJq','2019-01-26 18:03:21','2019-01-26 18:03:21'),(43,'Kevin Doquero 10','kevin.doque.10@yopmail.com','$2b$12$QfTyRbJm.QVsllbJ7t57JOtrubfo5FzvaMz6aFcZwR1.sp17g3oJq','2019-01-26 18:03:21','2019-01-26 18:03:21'),(44,'Kevin Doquero 11','kevin.doque.11@yopmail.com','$2b$12$QfTyRbJm.QVsllbJ7t57JOtrubfo5FzvaMz6aFcZwR1.sp17g3oJq','2019-01-26 18:03:21','2019-01-26 18:03:21'),(45,'Kevin Doquero 12','kevin.doque.12@yopmail.com','$2b$12$QfTyRbJm.QVsllbJ7t57JOtrubfo5FzvaMz6aFcZwR1.sp17g3oJq','2019-01-26 18:03:21','2019-01-26 18:03:21'),(46,'Kevin Doquero 13','kevin.doque.13@yopmail.com','$2b$12$QfTyRbJm.QVsllbJ7t57JOtrubfo5FzvaMz6aFcZwR1.sp17g3oJq','2019-01-26 18:03:21','2019-01-26 18:03:21'),(47,'Kevin Doquero 14','kevin.doque.14@yopmail.com','$2b$12$QfTyRbJm.QVsllbJ7t57JOtrubfo5FzvaMz6aFcZwR1.sp17g3oJq','2019-01-26 18:03:21','2019-01-26 18:03:21'),(48,'admin','admin@yopmail.com','$2b$12$b3Vikr.zlpHMpa3jENZaq.NNor6p6ZTdOO39GdzZCQzuG/FkuGEfi','2019-01-26 18:03:22','2019-01-26 18:03:22');
/*!40000 ALTER TABLE `Users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-09 16:08:25
