require('dotenv').config()

module.exports = {
  "development": {
    "username": process.env.DB_USER,
    "password": process.env.DB_PASS,
    "database": process.env.DB_NAME,
    "host": "127.0.0.1",
    "dialect": "mysql",
    "port": process.env.DB_PORT,

  },
  "test": {
    "username": "alpha",
    "password": "yaghouba",
    "database": "my_eDatabase",
    "host": "127.0.0.1",
    "dialect": "mysql",
    "port": "3308"
  },
  "production": {
    "username": "alpha",
    "password": "yaghouba",
    "database": "my_eDatabase",
    "host": "127.0.0.1",
    "dialect": "mysql",
    "port": "3308"

  }
}
